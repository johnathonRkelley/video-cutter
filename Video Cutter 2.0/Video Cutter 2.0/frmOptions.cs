﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Video_Cutter_2._0
{
    public partial class frmOptions : Form
    {
        public frmMain form1; //Gets Form1's methods to control the main form through this form.

        /* Options Form Main Methods */
        public frmOptions(frmMain form1)
        {
            this.form1 = form1;
            InitializeComponent();

            txtLocation.Text = form1.getFileLocation();
        }

        /* Button Calls */
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            fbd.Description = "Please Select Clip Location";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtLocation.Text = fbd.SelectedPath;
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
