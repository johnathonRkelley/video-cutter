﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Video_Cutter_2._0
{
    class Json
    {
        private String path;

        public Json()
        {
            string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            path = appPath + "\\path.txt";
        }

        //Loads the data from the text file.
        public User loadDataFromFile()
        {
            string jsonFromFile = System.IO.File.ReadAllText(path);
            User user = JsonConvert.DeserializeObject<User>(jsonFromFile);
            return user;
        }
        //Saves the data to the text file
        public void saveTheData(User user)
        {
            string json = JsonConvert.SerializeObject(user);

            System.IO.File.WriteAllText(path, json);
        }
    }
}
