﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Video_Cutter_2._0
{
    public partial class frmMain : Form
    {
        List<String[]> videosToBeProcessed;
        public double startTime, endTime;
        public string fileOutputLocation, ffmpegFileLocation;
        public int seconds;
        User user;

        /* Main Form Methods */
        public frmMain()
        {
            InitializeComponent();
            initializingGlobalVariables();
        }

        public void initializingGlobalVariables()
        {
            videosToBeProcessed = new List<String[]>();
            startTime = 0;
            endTime = 0;
            seconds = 0;
            fileOutputLocation = "";
            ffmpegFileLocation = '\u0022' + Application.StartupPath + "\\" + "ffmpeg.win32.exe" + '\u0022';
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            loadingJSONDataFromFile();
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            new Json().saveTheData(new User("Test Profile", fileOutputLocation, getVideosFromListView()));
        }

        /* Button Click Methods */
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Browse Video Files",

                CheckFileExists = true,
                CheckPathExists = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true,

                Filter = "Video Files (*.mp4)|*.mp4"
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String filePath = openFileDialog1.FileName;
                new frmVideoViewer(this, filePath).Show();
            } 
        }
        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (isAllTextBoxesEmpty())
            {
                lblTest.Text = "Please make sure all of values are filled in above.";
            }
            else
            {
                String[] arr = { txtFilePath.Text, txtClipName.Text, startTime.ToString(), endTime.ToString()};
                listViewBuilder(arr);

                lblTest.Text = "Clip for file : " + txtFilePath.Text + " has been added.";

                clearAllTextBoxes();
            }
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (isListViewEmpty())
            {
                lblTest.Text = "Please add videos into the listview above before removing.";
            }
            else
            {
                bool selected = false;

                for (int i = listView1.Items.Count - 1; i >= 0; i--)
                {
                    if (listView1.Items[i].Selected)
                    {
                        selected = true;
                        listView1.Items[i].Remove();
                    }
                }

                if (selected == false)
                {
                    lblTest.Text = "Nothing was selected. Please select a video to remove from the listview.";
                }

            }
            
        }
        private void btnRun_Click(object sender, EventArgs e)
        {
            if (isListViewEmpty())
            {
                lblTest.Text = "Please add videos to be clipped above";
            }
            else
            {
                lblTest.Text = "Starting Clipping Process";

                getVideosFromListView();

                timer1.Enabled = true;
            }
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        /* Builds ListView Using array*/
        public void listViewBuilder(String[] arr)
        {
            listView1.Items.Add(new ListViewItem(arr));
        }

        public List<Video> getVideosFromListView()
        {
            List<Video> videos = new List<Video>();
            foreach (ListViewItem anItem in listView1.Items)
            {
                String[] arr = new String[4];
                arr[0] = anItem.SubItems[0].Text; //Filename
                arr[1] = fileOutputLocation + "\\" + anItem.SubItems[1].Text; //CLipNamean
                arr[2] = anItem.SubItems[2].Text; //Start
                arr[3] = anItem.SubItems[3].Text; //Stop

                videos.Add(new Video(arr[0], anItem.SubItems[1].Text, arr[2], arr[3]));
                
                videosToBeProcessed.Add(arr);
            }
            return videos;
        }

        /* Reading/Writing to Text File */
        public void loadingJSONDataFromFile()
        {
            user = new Json().loadDataFromFile();

            fileOutputLocation = user.options_filepath;
            List<Video> videos = user.getVideos();

            foreach (Video video in videos)
            {
                listViewBuilder(video.getArrayData());
            }
        }

        /* Form Methods From Outside of Form 1 */
        public void setStartTime(int time)
        {
            txtStart.Text = time.ToString();
            startTime = time;
        }
        public void setEndTime(int time)
        {
            txtStop.Text = time.ToString();
            endTime = time;
        }

        public void setClipName(String name){
            txtClipName.Text = name;
        }
        public void setFilePath(String path)
        {
            txtFilePath.Text = path;
        }

        public void clearAllTextBoxes()
        {
            txtStart.Text = "";
            txtStop.Text = "";
            txtClipName.Text = "";
            txtFilePath.Text = "";
        }

        /* Utilizes CMD and ffmpeg to clip the videos */
        public void clippingProcess(String[] videoData)
        {
            int totalTimeElapsed = Int32.Parse(videoData[3]) - Int32.Parse(videoData[2]); 
            Process p = new Process();
            p.StartInfo.UseShellExecute = false; // ensures you can read stdout
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = ffmpegFileLocation;
            //p.StartInfo.Arguments = " -y -ss " + currentVideo.getStart() + " -i " + currentVideo.getFilename() + " -t " + (currentVideo.getStop() - currentVideo.getStart()) + " -c copy " + currentVideo.getClip();

            p.StartInfo.Arguments = " -y -ss " + videoData[2] + " -i " + '\u0022' + videoData[0] + '\u0022' + " -t " + totalTimeElapsed + " -c copy " + '\u0022' + videoData[1] + ".mp4" +  '\u0022';
            Console.WriteLine(ffmpegFileLocation + " " + p.StartInfo.Arguments);
            p.Start();
        }

        /* Menu Bar Methods */
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOptions(this).Show();
        }
        public void setFileLocation(String location)
        {
            fileOutputLocation = location;
        }
        public String getFileLocation()
        {
            return fileOutputLocation;
        }

        /* Timer for Processing Video Files */
        private void timer1_Tick(object sender, EventArgs e)
        {
            seconds++;

            if (isSecondsEqualToFive() && isVideosToBeProcessNotEqualToZero())
            {
                String[] temp = videosToBeProcessed[0];
                lblTest.Text = "Clipping : " + temp[0];
                clippingProcess(temp);
                videosToBeProcessed.RemoveAt(0);
                seconds = 0;
            }

            if (isVideosToBeProcessedEmpty())
            {
                timer1.Enabled = false;
                lblTest.Text = "All files have been clipped. Files Located in : " +fileOutputLocation;
                listView1.Clear();
            }
        }

        /* True/False */
        public bool isVideosToBeProcessedEmpty()
        {
            if (videosToBeProcessed.Count == 0)
                return true;
            else
                return false;
        }

        public bool isVideosToBeProcessNotEqualToZero()
        {
            if (videosToBeProcessed.Count > 0)
                return true;
            else
                return false;
        }

        public bool isSecondsEqualToFive()
        {
            if (seconds == 5)
                return true;
            else
                return false;
        }

        public bool isAllTextBoxesEmpty()
        {
            if (txtFilePath.Text.Equals("") || txtClipName.Text.Equals("") || txtStart.Text.Equals("") || txtStop.Text.Equals(""))
                return true;
            else
                return false;
        }

        public bool isListViewEmpty()
        {
            if (listView1.Items.Count == 0)
                return true;
            else
                return false;
        }
    }
}
