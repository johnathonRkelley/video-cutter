﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Video_Cutter_2._0
{
    public class User
    {
        [JsonProperty("profile_name")]
        public String profile_name { get; set; }
        [JsonProperty("options_filepath")]
        public String options_filepath { get; set; }
        [JsonProperty("videos")]
        public List<Video> videos { get; set; }

        public User()
        {

        }

        public User(String profile, String options, List<Video> videos)
        {
            profile_name = profile;
            options_filepath = options;
            this.videos = videos;
        }

        public List<Video> getVideos()
        {
            return videos;
        }

        public void addVideos(Video video)
        {
            videos.Add(video);
        }
    }
}
