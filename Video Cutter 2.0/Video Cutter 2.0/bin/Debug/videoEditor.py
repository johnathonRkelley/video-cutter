from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import sys

class Video:
    video_name = ""
    start_time = ""
    end_time = ""
    # instance attribute
    def __init__(self, video_name, start_time, end_time):
        self.video_name = video_name
        self.start_time = start_time
        self.end_time = end_time
    
    def getData(self):
        print(self.video_name + " " + self.start_time + " " + self.end_time)

print("Going into python code")
print(sys.argv[1])
print(sys.argv[2])
print(sys.argv[3])
print(sys.argv[4]) 
#Another arg file directory to place videos
#Read text files from FILENAME, START, FINISH

#FILENAME and TARGETNAME = FILENAME-OUTPUT
#filepath = "C:\\Users\\Johnathon\\Videos\\"
#data = filepath + sys.argv[4] + ".mp4"
data = sys.argv[4] + ".mp4"
ffmpeg_extract_subclip(sys.argv[1], int(sys.argv[2]) , int(sys.argv[3]), targetname=data)
#ffmpeg_extract_subclip(r"C:\Users\Johnathon\Videos\2019-04-16 02-03-11.mp4", 0, 20, targetname="test.mp4")