﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Video_Cutter_2._0
{
    public partial class TestingClickedListItem : Form
    {

        public String checkMark = "✓";
        public TestingClickedListItem()
        {
            InitializeComponent();

            listView1.Columns.Add("File Name", 600);
            listView1.Columns.Add("Checked", 100);
            listView1.Columns.Add("Clipped", 100);
            listView1.Columns[1].TextAlign = HorizontalAlignment.Center;
            listView1.Columns[2].TextAlign = HorizontalAlignment.Center;

            listView1.Items.Add(new ListViewItem(new String[] { "Test Location", "X", "X" }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkSpecifiedBox(1);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            checkSpecifiedBox(2);
        }

        public void checkSpecifiedBox(int location)
        {
            // 1 - Checked Location
            // 2 - Clipped Location

            for (int i = listView1.Items.Count - 1; i >= 0; i--)
            {
                if (listView1.Items[i].Selected)
                {
                    Console.WriteLine(listView1.Items[i].SubItems[2].Text);

                    listView1.Items[i].SubItems[location].Text = checkMark;
                }
            }
        }
    }
}
