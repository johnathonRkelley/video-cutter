﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Video_Cutter_2._0
{
    public partial class frmVideoViewer : Form
    {
        public frmMain form1;
        String path;
        int start, stop;
        public frmVideoViewer(frmMain form1, String filename)
        {
            InitializeComponent();
            mediaPlayer.uiMode = "full";
            this.form1 = form1;
            path = filename;
            start = 0;
            stop = 0;
            //string path = Application.StartupPath + "\\" + "test.mp4";
            this.Text = "Video Viewer | " + filename;
            mediaPlayer.URL = path;
            mediaPlayer.settings.volume = 0;
        }

        private void VideoViewer_Load(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            double time = mediaPlayer.Ctlcontrols.currentPosition;
            int timeCut = (int)time;
            label1.Text = timeCut.ToString();
            start = timeCut;
        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            double time = mediaPlayer.Ctlcontrols.currentPosition;
            int timeCut = (int)time;
            label2.Text = timeCut.ToString();
            stop = timeCut;
        }

        private void btnAddInfo_Click(object sender, EventArgs e)
        {
            form1.setStartTime(start);
            form1.setEndTime(stop);
            form1.setClipName(txtClipName.Text);
            form1.setFilePath(path);
            this.Close();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
