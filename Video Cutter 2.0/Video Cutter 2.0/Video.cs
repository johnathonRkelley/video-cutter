﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Video_Cutter_2._0
{
    public class Video
    {
        public String filename;
        public int startSeconds;
        public int stopSeconds;
        public String clipName;

        /* Constructors */
        public Video()
        {

        }

        public Video(String file, String clip, String start, String stop)
        {
            filename = file;
            startSeconds = Int32.Parse(start);
            stopSeconds = Int32.Parse(stop);
            clipName = clip; 
        }

        /* Getters / Setters */
        public String getData()
        {
            return filename + "/" + startSeconds + "/" + stopSeconds;
        }
        public int getStart()
        {
            return startSeconds;
        }
        public int getStop()
        {
            return stopSeconds;
        }
        public String getFilename()
        {
            return '\u0022' + filename + '\u0022';
        }
        public String getClip()
        {
            return '\u0022' + clipName + ".mp4" + '\u0022';
        }
        public String getFullData()
        {
            return filename + "," + startSeconds + "," + stopSeconds + "," + clipName;
        }
        public String[] getArrayData()
        {
            String[] arr = new String[4];
            arr[0] = filename;
            arr[1] = clipName;
            arr[2] = startSeconds.ToString();
            arr[3] = stopSeconds.ToString();
            return arr;
        }
    }
}
